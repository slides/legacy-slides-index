---
title: Oxford Geek Nights 52 - How Robots See Fiducial Markers
date: 2022-11-16
description: Robots are everywhere, and so are cameras. When a computer needs to track an object, it might use a marker to help. But how does that all work?
slides: https://slides.gitlab-pages.theorangeone.net/ogn-52-fiducial-marker-detection/
tags:
    - ogn
---
